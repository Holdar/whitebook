//
//  PersistenceManager.swift
//  WhiteBook
//
//  Created by Murtazaliev Shamil on 31/05/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import Foundation
import CoreData

final class PersistenceManager {
    
    private init(){}
    
    static let shared = PersistenceManager()
    
    lazy var persistenceContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy var context = persistenceContainer.viewContext
    
    func saveContext() {
//        let context = persistenceContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            }catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
