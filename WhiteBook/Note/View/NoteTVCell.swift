//
//  NoteTVCell.swift
//  WhiteBook
//
//  Created by Murtazaliev Shamil on 29/05/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import UIKit

class NoteTVCell: UITableViewCell {

    @IBOutlet weak var noteImage: UIImageView!
    @IBOutlet weak var noteLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
