//
//  ViewController.swift
//  WhiteBook
//
//  Created by Murtazaliev Shamil on 27/05/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import UIKit
import CoreData

struct objectNote {
    var sectionName: String!
    var sectionCell: [String]!
}

final class NoteVC: UIViewController, NoteViewInput{
    
    var array: [objectNote] = [objectNote(sectionName: "s1", sectionCell: ["1пупуып", "2павп", "3рпува"]), objectNote(sectionName: "s2", sectionCell: ["4павпа", "5павп", "6павп"]), objectNote(sectionName: "s3", sectionCell: ["7павп", "8куп", "9папуыап"])]
    
    var output: NoteViewOutput!
    
    var persistenceManager: PersistenceManager!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        persistenceManager = PersistenceManager.shared
        
        output = NotePresenter(view: self)
        createNote()
        getNote()
    }
    
    func createNote() {
        let note = Note(context: persistenceManager.context)
        note.noteTitle = ".,mnnbvvcxxzasdfghjkl;'[poiuytrewglmdfgnjdfsnjosdnhrnhkjcirnhrcuirhgubnrhingbirhnbhinhinrhgrh xvckmnbfgd bk knsg bdfg knbjdgsjdsnjdngjkdngkjsdfng.,mnnbvvcxxzasdfghjkl;'[poiuytrewglmdfgnjdfsnjosdnhrnhkjcirnhrcuirhgubnrhingbirhnbhinhinrhgrh xvckmnbfgd bk knsg bdfg knbjdgsjdsnjdngjkdngkjsdfng.,mnnbvvcxxzasdfghjkl;'[poiuytrewglmdfgnjdfsnjosdnhrnhkjcirnhrcuirhgubnrhingbirhnbhinhinrhgrh xvckmnbfgd bk knsg bdfg knbjdgsjdsnjdngjkdngkjsdfng.,mnnbvvcxxzasdfghjkl;'[poiuytrewglmdfgnjdfsnjosdnhrnhkjcirnhrcuirhgubnrhingbirhnbhinhinrhgrh xvckmnbfgd bk knsg bdfg knbjdgsjdsnjdngjkdngkjsdfng.,mnnbvvcxxzasdfghjkl;'[poiuytrewglmdfgnjdfsnjosdnhrnhkjcirnhrcuirhgubnrhingbirhnbhinhinrhgrh xvckmnbfgd bk knsg bdfg knbjdgsjdsnjdngjkdngkjsdfng.,mnnbvvcxxzasdfghjkl;'[poiuytrewglmdfgnjdfsnjosdnhrnhkjcirnhrcuirhgubnrhingbirhnbhinhinrhgrh xvckmnbfgd bk knsg bdfg knbjdgsjdsnjdngjkdngkjsdfng"
        note.noteDescription = "asdfghjkl"
        
        print(".,mnnbvvcxxzasdfghjkl;'[poiuytrewglmdfgnjdfsnjosdnhrnhkjcirnhrcuirhgubnrhingbirhnbhinhinrhgrh xvckmnbfgd bk knsg bdfg knbjdgsjdsnjdngjkdngkjsdfng")
    }
    
    func getNote() {
        guard let notes = try! persistenceManager.context.fetch(Note.fetchRequest()) as? [Note] else {
            return
        }
        
        notes.forEach({ print($0.noteTitle ?? "12345678900988678356735543654634565475757")})
    }
}


extension NoteVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array[section].sectionCell.count
    } 
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.orange
        
        let lable = UILabel()
        lable.text = array[section].sectionName
        print("\( self.view.bounds.width - lable.bounds.width )")
        lable.frame = CGRect(x:5 , y: 5, width: 30, height: 30)
        lable.font = lable.font.withSize(20)
        view.addSubview(lable)

        let button = UIButton()
        button.frame = CGRect(x: self.view.bounds.width - 30, y: 10, width: 20, height: 20)
        button.setImage(UIImage(named: "note"), for: .normal)
        button.addTarget(self, action: #selector(addCell), for: .touchUpInside)
        view.addSubview(button)
    
        return view
    }
    
    @objc func addCell() {
        print("addCell")
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(40)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? NoteTVCell else { fatalError()}
        
        cell.noteLabel.text = array[indexPath.section].sectionCell[indexPath.row]
        
        return cell
    }
    
}
