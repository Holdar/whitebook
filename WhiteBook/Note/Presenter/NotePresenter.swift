//
//  PresenterNote.swift
//  WhiteBook
//
//  Created by Murtazaliev Shamil on 29/05/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import Foundation

final class NotePresenter: NoteViewOutput{
    
    weak var view: NoteViewInput!
    
    init(view: NoteViewInput) {
        self.view = view
    }
    
}
