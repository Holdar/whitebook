//
//  Note+CoreDataProperties.swift
//  WhiteBook
//
//  Created by Murtazaliev Shamil on 31/05/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//
//

import Foundation
import CoreData


extension Note {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Note> {
        return NSFetchRequest<Note>(entityName: "Note")
    }

    @NSManaged public var noteImage: NSData?
    @NSManaged public var noteTitle: String?
    @NSManaged public var noteDescription: String?

}
